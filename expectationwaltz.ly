
\header{
  title = "Expectation Waltz"
  subtitle = "Transcription of the viola part for double bass"
}

\relative {
	\clef tenor
	\time 3/4
	\key d \minor
	\compressFullBarRests


	R2.*3
	
%4
	r4 r a ||
	d2 d4
	a'2 cis,4
	d4. a8 gis a
	d4 r a
%9
	d a d 
	a d4. a8
	cis4. d8 e d
	cis4 r a 
	cis e a
	cis a cis
%15
	e,8 d d cis cis d
	e4 r a, 
	g' e cis
	a d e 
	f2.~
	f8 bes a g f e
%21
	bes2 a4 
	f2 e4
	d' a f 
	d r f8 bes
	d4 r a
	d r a8 a
%27
	d,4 f a 
	d bes d
	g8 e cis a cis e
	cis4 cis a
	f'8 e d a f a
	d4 bes a
%33
	cis2 d4
	cis'2 e,4
	d f a
	d r r
%37
	r8 a, b cis d e
	f4 r r
	r8 a, b cis d e
	f4 r r
	r8 g, bes d e g
	bes4 r r
%43
	r8 g, bes d e g
	bes4 a, a
	e' cis e
	a8 r a4 g
	f a, d
	f bes, d
%49

%55

%61

%67


}


\version "2.18.2"  % necessary for upgrading to future LilyPond versions.
